package com.agiletestingalliance;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import javax.servlet.http.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.mockito.Mockito;

public class CPDOFexam2Test extends Mockito{

    @Test
    public void testMinNumber() throws Exception {

        int k= new MinNumber().compareNumbers(1,2);
        assertEquals("Compare",2,k);
        
    }

    @Test
    public void testAboutCPDOF() throws Exception {

        String aboutCPDOFString = new AboutCPDOF().desc();
        assertTrue(aboutCPDOFString.contains("CP-DOF"));
        
    }

    @Test
    public void testDuration() throws Exception {

        String durationString = new Duration().dur();
        assertTrue(durationString.contains("CP-DOF"));
        
    }

    @Test
    public void testUsefulness() throws Exception {

        String usefullnessString = new Usefulness().desc();
        assertTrue(usefullnessString.contains("DevOps"));
        
    }

}
